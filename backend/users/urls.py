# -*- coding: utf-8 -*-
"""Root url routering file.

You should put the url config in their respective app putting only a
refernce to them here.
"""

# Third Party Stuff
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

# Top Level Pages
# ==============================================================================
urlpatterns = [
    # Your stuff: custom urls go here
]

