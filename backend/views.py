from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.forms import (
    AuthenticationForm, PasswordChangeForm, PasswordResetForm, SetPasswordForm,
)
from django.contrib.auth.tokens import default_token_generator
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache

from backend.users.models import User

from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import viewsets, status

import json

@api_view(['POST'])
def check_if_email_is_available(request):
    """Check if email is available"""
    email = request.POST['email']
    email_exists = User.objects.filter(email=email).exists()
    return Response({'available': email_exists})


# @csrf_exempt
@api_view(['GET', 'POST'])
def user_login(request):
    """View to log a user in."""
    post_data = request.POST
    email = post_data.get('email')
    password = post_data.get('password')
    user = authenticate(email=email, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            response = Response({"message": "Logged in successfuly"},
                            status=status.HTTP_200_OK)
            response.set_cookie("login", "true")
            return response
        else:
            return Response({"message": "Permission Denied"},
                            status=status.HTTP_200_OK)
    else:
        return Response({"error": "Invalid login details provided"},
                        status=status.HTTP_200_OK)


def logout(request):
    """Logout a user."""
    logout(request)
    response = Response({"message": "Logged out successfuly"},
                    status=status.HTTP_200_OK)
    response.set_cookie("login", "false")
    return response


# @csrf_exempt
def signup(request):
    """View for user sign up."""
    if request.method == "GET":
        # send the form to the user
        return render_to_response("signup.html", {},
                                  context_instance=RequestContext(request))

    else:
        email = request.POST['email']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        password = request.POST['password']
        # dob = request.POST["dob"]
        email_exists = User.objects.filter(email=email).exists()
        if email_exists:
            return HttpResponse(json.dumps({'error': "email not available"}),
                                content_type="application/json")

        #try:
        user = User.objects.create(email=email,
                                   first_name=first_name,
                                   last_name=last_name)
        user.set_password(password)
        user.save()
        user = authenticate(email=email, password=password)
        login(request, user)
        response = HttpResponse(json.dumps({"message": "Signup successful"}),
                       content_type="application/json") 
        return response
        #except:
        #    response = HttpResponse(json.dumps({"error": "Can't signup"}),
        #                   content_type="application/json") 
        #    return response

@api_view(['GET'])
def get_user_name(request):
    """Return name of user"""
    if request.user.is_authenticated():
        try:
            user = request.user
            name = user.get_full_name()
            return Response({"name": name}, status=status.HTTP_200_OK)
        except User.DoesNotExist:
            return Response({"error": "Invalid user"}, status=status.HTTP_200_OK)

    else:
        return Response({"error": "Unauthenticated user"}, status=status.HTTP_200_OK)


@csrf_protect
def password_reset(request, is_admin_site=False,
                   template_name='registration/password_reset_form.html',
                   email_template_name='registration/password_reset_email.html',
                   subject_template_name='registration/password_reset_subject.txt',
                   password_reset_form=PasswordResetForm,
                   token_generator=default_token_generator,
                   post_reset_redirect=None,
                   from_email=None,
                   extra_context=None,
                   html_email_template_name=None,
                   extra_email_context=None):
    if post_reset_redirect is None:
        post_reset_redirect = reverse('password_reset_done')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    if request.method == "POST":
        form = password_reset_form(request.POST)
        if form.is_valid():
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                'from_email': from_email,
                'email_template_name': email_template_name,
                'subject_template_name': subject_template_name,
                'request': request,
                'html_email_template_name': html_email_template_name,
                'extra_email_context': extra_email_context,
            }
            form.save(**opts)
            return HttpResponseRedirect(post_reset_redirect)


def password_reset_done(request,
                        template_name='registration/password_reset_done.html',
                        extra_context=None):
    context = {
        'title': _('Password reset sent'),
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


# Doesn't need csrf_protect since no-one can guess the URL
@sensitive_post_parameters()
@never_cache
def password_reset_confirm(request, uidb64=None, token=None,
                           template_name='registration/password_reset_confirm.html',
                           token_generator=default_token_generator,
                           set_password_form=SetPasswordForm,
                           post_reset_redirect=None,
                           extra_context=None):
    """
    View that checks the hash in a password reset link and presents a
    form for entering a new password.
    """
    UserModel = get_user_model()
    assert uidb64 is not None and token is not None  # checked by URLconf
    if post_reset_redirect is None:
        post_reset_redirect = reverse('password_reset_complete')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    try:
        # urlsafe_base64_decode() decodes to bytestring on Python 3
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = UserModel._default_manager.get(pk=uid)
    except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    if user is not None and token_generator.check_token(user, token):
        validlink = True
        title = _('Enter new password')
        if request.method == 'POST':
            form = set_password_form(user, request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(post_reset_redirect)
        else:
            form = set_password_form(user)
    else:
        validlink = False
        form = None
        title = _('Password reset unsuccessful')
    context = {
        'form': form,
        'title': title,
        'validlink': validlink,
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


def password_reset_complete(request,
                            template_name='registration/password_reset_complete.html',
                            extra_context=None):
    context = {
        'login_url': resolve_url(settings.LOGIN_URL),
        'title': _('Password reset complete'),
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)

