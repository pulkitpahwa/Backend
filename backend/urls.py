# -*- coding: utf-8 -*-
"""Root url routering file.

You should put the url config in their respective app putting only a
refernce to them here.
"""

# Third Party Stuff
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

# Top Level Pages
# ==============================================================================
urlpatterns = [
    # Your stuff: custom urls go here
    url(r'^api/signup$', 'backend.views.signup'),
    url(r'^api/login$', 'backend.views.login'),
    url(r'^api/logout$', 'backend.views.logout'),
    url(r'^api/get_user_name$', 'backend.views.get_user_name'),
    url(r'^api/user/', include('backend.users.urls')),
]

urlpatterns += [

    url(r'^api/auth-n/', include('rest_framework.urls', namespace='rest_framework')),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
